# -*- coding: utf-8 -*-
"""
    common.execute
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to execute system processes
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import platform

def run_process(cmd):
    return os.system(cmd)


def run_process_safe(cmd):
    exit_code = os.system(cmd)
    if exit_code != 0:
        raise Exception("Could not execute " + cmd)


def read_process(cmd):
    with os.popen(cmd) as f:
        return f.read()


def is_linux():
    return platform.system() == "Linux"


def is_windows():
    return platform.system() == "Windows"


def is_macos():
    return platform.system() == "Darwin"
