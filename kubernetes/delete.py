# -*- coding: utf-8 -*-
"""
    component.delete
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to delete a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

from kubernetes import delete_resources
from kubernetes import delete_service

def delete(config_path, resources_path):
    delete_service(config_path, resources_path)
    delete_resources(config_path, resources_path)