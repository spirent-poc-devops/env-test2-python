# -*- coding: utf-8 -*-
"""
    component.terraform
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to process terraform files
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import shutil
from common import sync_terraform_state
from common import get_map_value, set_map_value

def env_keypair_terraform(config, resources):
    vars_str = "-var aws_cred_file=" + get_map_value(config, "aws.credentials_file") + \
        " -var aws_region=" + get_map_value(config, "aws.region") + \
        " -var aws_profile=" + get_map_value(config, "aws.profile") + \
        " -var aws_stage=" + get_map_value(config, "aws.stage") + \
        " -var aws_namespace=" + get_map_value(config, "aws.namespace") + \
        " -var env_nameprefix=" + get_map_value(config, "environment.prefix")
    
    return vars_str

def init_keypair_terraform(environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    keypair_terraform_path = path + "/../temp/keypair_k8s_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(keypair_terraform_path):
        os.mkdir(keypair_terraform_path)

    # Load terraform state
    sync_terraform_state("keypair_k8s", environment_prefix)

    # Copy terraform scripts to terraform folder
    shutil.copy(path + "/templates/terraform_scripts/provider.tf", keypair_terraform_path)
    shutil.copy(path + "/templates/terraform_scripts/keypair.tf", keypair_terraform_path)

    return keypair_terraform_path


def env_k8s_terraform(config, resources):
    vars_str = "-var aws_cred_file=" + get_map_value(config, "aws.credentials_file") + \
        " -var aws_region=" + get_map_value(config, "aws.region") + \
        " -var aws_profile=" + get_map_value(config, "aws.profile") + \
        " -var aws_vpc=" + get_map_value(config, "aws.vpc") + \
        " -var k8s_instance_ami=" + get_map_value(config, "k8s.instance_ami") + \
        " -var k8s_instance_type=" + get_map_value(config, "k8s.instance_type") + \
        " -var k8s_keypair_name=" + get_map_value(resources, "k8s.keypair_name") + \
        " -var k8s_subnet_id=" + get_map_value(config, "k8s.subnet_id") + \
        " -var k8s_volume_size=" + str(get_map_value(config, "k8s.volume_size")) + \
        " -var k8s_associate_public_ip_address=" + get_map_value(config, "k8s.associate_public_ip_address") + \
        " -var k8s_ebs_snapshot_id=" + get_map_value(config, "k8s.ebs_snapshot_id") + \
        " -var public_hosted_zone_id=" + get_map_value(config, "environment.route53_zone_id") + \
        " -var dns_public_name=" + get_map_value(config, "environment.prefix") + "." + get_map_value(config, "environment.public_domain") + \
        " -var env_nameprefix=" + get_map_value(config, "environment.prefix") + \
        " -var mgmt_subnet_cidr=" + get_map_value(config, "mgmt_station.subnet_cidr") + \
        " -var mgmt_public_ip=" + get_map_value(resources, "mgmt_station.public_ip")
    
    return vars_str

    
def init_k8s_terraform(config, environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    keypair_terraform_path = path + "/../temp/k8s_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(keypair_terraform_path):
        os.mkdir(keypair_terraform_path)

    # Load terraform state
    sync_terraform_state("k8s", environment_prefix)

    # Copy terraform scripts to terraform folder
    shutil.copy(path + "/templates/terraform_scripts/provider.tf", keypair_terraform_path)
    if get_map_value(config, "k8s.create_route53_ingress"):
        shutil.copy(path + "/templates/terraform_scripts/k8s_vm.tf", keypair_terraform_path)
    else:
        shutil.copy(path + "/templates/terraform_scripts/k8s_vm_without_route53.tf", keypair_terraform_path)

    return keypair_terraform_path