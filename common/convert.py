# -*- coding: utf-8 -*-
"""
    common.convert
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Data conversion functions
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import math

def get_aws_output(input):
    if input == None:
        return None

    # define keys fo key and value 
    key_param = 'OutputKey'
    value_param = 'OutputValue'
    result = dict()

    for item in input:
        new_key = item[key_param] if key_param in item else None
        new_value = item[value_param] if value_param in item else None

        if new_key != None:
            result[new_key] = new_value
 
    return result

