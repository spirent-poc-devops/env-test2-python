# -*- coding: utf-8 -*-
"""
    component.create
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

from kubernetes import create_resources
from kubernetes import create_service

def create(config_path, resources_path):
    create_resources(config_path, resources_path)
    create_service(config_path, resources_path)