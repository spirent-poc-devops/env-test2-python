# -*- coding: utf-8 -*-
"""
    common.terraform
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Functions to handle terraform templates
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import shutil
from .logging import write_error, write_info

def save_terraform_state(component, env_prefix):
    path = os.path.abspath(os.path.dirname(__file__))

    # Set tfstate pathes
    tmp_state_path = path + "/../temp/" + component + "_" + env_prefix + "/terraform.tfstate"
    cfg_state_path = path + "/../config/" + component + "_" + env_prefix + "_terraform.tfstate"

    # Copy state to config folder
    if os.path.exists(tmp_state_path):
        shutil.copyfile(tmp_state_path, cfg_state_path)
    else:
        write_info(component, "Can't find terraform state - " + tmp_state_path)


def sync_terraform_state(component, env_prefix):
    path = os.path.abspath(os.path.dirname(__file__))

    # Set tfstate pathes
    tmp_state_path = path + "/../temp/" + component + "_" + env_prefix + "/terraform.tfstate"
    cfg_state_path = path + "/../config/" + component + "_" + env_prefix + "_terraform.tfstate"

    # Copy state to config folder
    if os.path.exists(cfg_state_path):
        shutil.copyfile(cfg_state_path, tmp_state_path)
    else:
        write_info(component, cfg_state_path + " doesn't exists, loading terraform state skipped.")


def remove_terraform_state(component, env_prefix):
    path = os.path.abspath(os.path.dirname(__file__))

    # Set tfstate pathes
    cfg_state_path = path + "/../config/" + component + "_" + env_prefix + "_terraform.tfstate"

    # Delete state from config folder
    if os.path.exists(cfg_state_path):
        os.remove(cfg_state_path)
