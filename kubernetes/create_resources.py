# -*- coding: utf-8 -*-
"""
    component.create_resources
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create component computing instances
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import json
from common import read_config, read_resources, write_resources
from common import write_error, write_info
from common import get_map_value, set_map_value
from common import run_process, read_process
from common import save_terraform_state
from kubernetes import env_keypair_terraform, init_keypair_terraform, env_k8s_terraform, init_k8s_terraform

def create_resources(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    environment_prefix = get_map_value(config, "environment.prefix")
    path = os.path.abspath(os.path.dirname(__file__))

    # ### Keypair ###

    # Set terraform env variables and init terraform folder
    tf_vars = env_keypair_terraform(config, resources)
    keypair_terraform_path = init_keypair_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(keypair_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't initialize terraform. Watch logs above or check " + keypair_terraform_path +" folder content.")

    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't execute terraform plan. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    exit_code = run_process("terraform apply -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't create cloud resources. Watch logs above or check " + keypair_terraform_path +" folder content.")

    # Get terraform outputs
    outputs = json.loads(str(read_process("terraform output -json")))
    keypair_name = outputs["keypair_name"]["value"]

    os.chdir(path + "/..")

    save_terraform_state("keypair_k8s", environment_prefix)

    set_map_value(resources, "k8s.keypair_name", keypair_name)
    write_resources(resources_path, resources)

    # ### Kubernetes Virtual Machine ###

    # Set terraform env variables and init terraform folder
    tf_vars = env_k8s_terraform(config, resources)
    k8s_terraform_path = init_k8s_terraform(config, environment_prefix)

    # Run terraform scripts
    os.chdir(k8s_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't initialize terraform. Watch logs above or check " + k8s_terraform_path +" folder content.")

    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't execute terraform plan. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    exit_code = run_process("terraform apply -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't create cloud resources. Watch logs above or check " + k8s_terraform_path +" folder content.")

    # Get terraform outputs
    outputs = json.loads(str(read_process("terraform output -json")))
    k8s_id = outputs["k8s_id"]["value"]
    k8s_private_ip = outputs["k8s_private_ip"]["value"]
    k8s_public_ip = outputs["k8s_public_ip"]["value"]
    k8s_sg_id = outputs["k8s_sg_id"]["value"]

    os.chdir(path + "/..")

    save_terraform_state("k8s", environment_prefix)

    # Open self public ip access
    os.environ["AWS_DEFAULT_REGION"] = get_map_value(config, "aws.region")
    run_process("aws ec2 authorize-security-group-ingress --group-id "+ k8s_sg_id + \
        " --protocol tcp --port 0-65535 --cidr " + k8s_public_ip + "/32")

    # Save resources
    set_map_value(resources, "k8s.id", k8s_id)
    set_map_value(resources, "k8s.private_ip", k8s_private_ip)
    set_map_value(resources, "k8s.public_ip", k8s_public_ip)
    set_map_value(resources, "k8s.sg_id", k8s_sg_id)
    write_resources(resources_path, resources)
