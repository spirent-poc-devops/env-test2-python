# -*- coding: utf-8 -*-
"""
    component.delete
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to delete a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
from common import read_config, read_resources, write_resources
from common import get_map_value, remove_map_value
from common import build_template
from common import run_process

def delete(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    namespace = get_map_value(config, "k8s.namespace")
    template_params = {
        "namespace": namespace
    }

    # Set variables from config
    path = os.path.abspath(os.path.dirname(__file__))
    build_template(path + "/templates/namespace.yml", path + "/../temp/namespace.yml", template_params)

    # Create k8s namespace
    run_process('kubectl delete -f "' + path + '/../temp/namespace.yml"')

    # Save parameters into resources
    remove_map_value(resources, "k8s.namespace")
    write_resources(resources_path, resources)
