# -*- coding: utf-8 -*-
"""
    component.__init__
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Component module definition
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

__all__ = [
    'create', 'create_resources', 'create_service',
    'env_keypair_terraform', 'init_keypair_terraform', 'env_k8s_terraform', 'init_k8s_terraform', 'update',
    'delete', 'delete_resources', 'delete_service'
]

from .terraform import env_keypair_terraform, init_keypair_terraform, env_k8s_terraform, init_k8s_terraform
from .create import create
from .create_resources import create_resources
from .create_service import create_service
from .update import update
from .delete import delete
from .delete_resources import delete_resources
from .delete_service import delete_service