# -*- coding: utf-8 -*-
"""
    component.__init__
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Component module definition
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

__all__ = ['create', 'update', 'delete']

from .create import create
from .update import update
from .delete import delete