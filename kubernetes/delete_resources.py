# -*- coding: utf-8 -*-
"""
    component.delete_resources
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to delete component computing instances
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""
import os
from common import read_config, read_resources, write_resources
from common import get_map_value, remove_map_value, test_map_value
from common import remove_terraform_state
from common import write_error
from common import run_process
from kubernetes import env_keypair_terraform, init_keypair_terraform, env_k8s_terraform, init_k8s_terraform

def delete_resources(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    environment_prefix = get_map_value(config, "environment.prefix")
    path = os.path.abspath(os.path.dirname(__file__))

    # ### Kubernetes Virtual Machine ###

    # Set terraform env variables and init terraform folder
    tf_vars = env_k8s_terraform(config, resources)
    k8s_terraform_path = init_k8s_terraform(config, environment_prefix)

    # Run terraform scripts
    os.chdir(k8s_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't initialize terraform. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't execute terraform plan. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    exit_code = run_process("terraform destroy -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't delete cloud resources. Watch logs above or check " + k8s_terraform_path +" folder content.")
    
    os.chdir(path + "/..")

    remove_terraform_state("k8s", environment_prefix)

    if test_map_value(resources, "k8s"):
        remove_map_value(resources, "k8s.id")
        remove_map_value(resources, "k8s.private_ip")
        remove_map_value(resources, "k8s.public_ip")
        remove_map_value(resources, "k8s.sg_id")
        write_resources(resources_path, resources)

    # ### Keypair ###
    
    # Set terraform env variables and init terraform folder
    tf_vars = env_keypair_terraform(config, resources)
    keypair_terraform_path = init_keypair_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(keypair_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't initialize terraform. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't execute terraform plan. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    exit_code = run_process("terraform destroy -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("kubernetes", "Can't delete cloud resources. Watch logs above or check " + keypair_terraform_path +" folder content.")
    
    os.chdir(path + "/..")

    remove_terraform_state("keypair_k8s", environment_prefix)

    if test_map_value(resources, "k8s"):
        remove_map_value(resources, "k8s.keypair_name")
        write_resources(resources_path, resources)
