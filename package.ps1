#!/usr/bin/env pwsh

$component = Get-Content -Path "component.json" | ConvertFrom-Json
$packagePath = if ($null -ne $component.package_path) { $component.package_path } else { "dist" }
$packageExt = if ($null -ne $component.package_ext) { $component.package_ext } else { "zip" }
$releaseArchive = "$packagePath/$($component.name)-$($component.version)-$($component.build).$packageExt"
$latestArchive = "$packagePath/$($component.name)-latest.$packageExt"

# Clean up build directories
Get-ChildItem -Path "." -Include $packagePath -Recurse | ForEach-Object ($_) { Remove-Item -Force -Recurse $_.FullName }
New-Item -Path $packagePath -ItemType "Directory" -Force | Out-Null

$excludeFiles = @(
    ".git",
    ".gitignore",
    ".gitlab-ci.yml",
    $packagePath,
    "component.json",
    "build.ps1",
    "test.ps1",
    "package.ps1",
    "clean.ps1"
)

# Archive files
Get-ChildItem -Path "." -Exclude $excludeFiles -Recurse |
Compress-Archive -DestinationPath $releaseArchive -CompressionLevel "Optimal"

# Create latest archive
Copy-Item -Path $releaseArchive -Destination $latestArchive