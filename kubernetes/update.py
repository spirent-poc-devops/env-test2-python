# -*- coding: utf-8 -*-
"""
    component.update
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to update a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

from kubernetes import create

def update(config_path, resources_path):
    create(config_path, resources_path)
