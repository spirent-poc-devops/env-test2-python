# -*- coding: utf-8 -*-
"""
    component.terraform
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to process terraform files
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import glob, shutil
from common import sync_terraform_state
from common import get_map_value, set_map_value

def env_terraform(config, resources):
    vars_str = "-var env_prefix=" + get_map_value(config, "environment.prefix") + \
        " -var aws_region=" + get_map_value(config, "aws.region") + \
        " -var aws_credentials_file=" + get_map_value(config, "aws.credentials_file") + \
        " -var aws_profile=" + get_map_value(config, "aws.profile") + \
        " -var vpc=" + get_map_value(config, "aws.vpc") + \
        " -var instance_type=" + get_map_value(config, "mgmt_station.instance_type") + \
        " -var ami=" + get_map_value(config, "mgmt_station.instance_ami") + \
        " -var ami_owner=" + get_map_value(config, "mgmt_station.ami_owner") + \
        " -var subnet=" + get_map_value(config, "mgmt_station.subnet") + \
        " -var sg_id=" + get_map_value(config, "mgmt_station.sg_id") + \
        " -var root_volume_size=" + str(get_map_value(config, "mgmt_station.root_volume_size"))
    
    return vars_str


def init_terraform(environment_prefix):
    path = os.path.abspath(os.path.dirname(__file__))
    mgmt_terraform_path = path + "/../temp/mgmt_station_" + environment_prefix

    # Create terraform folder if not exists
    if not os.path.exists(mgmt_terraform_path):
        os.mkdir(mgmt_terraform_path)

    # Load terraform state
    sync_terraform_state("mgmt_station", environment_prefix)

    # Copy terraform scripts to terraform folder
    for f in glob.glob(path + "/terraform_scripts/*.tf"):
        shutil.copy(f, mgmt_terraform_path)

    return mgmt_terraform_path