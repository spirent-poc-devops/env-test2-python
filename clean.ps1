#!/usr/bin/env pwsh

$component = Get-Content -Path "component.json" | ConvertFrom-Json
$packagePath = if ($null -ne $component.package_path) { $component.package_path } else { "dist" }

# Clean up build directories
Get-ChildItem -Path "." -Include $packagePath -Recurse | ForEach-Object ($_) { Remove-Item -Force -Recurse $_.FullName }
