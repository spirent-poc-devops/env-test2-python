# -*- coding: utf-8 -*-
"""
    component.create
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    Function to create a component
    
    :copyright: Spirent Commynications 2021, see AUTHORS for more details.
    :license: ISC, see LICENSE for more details.
"""

import os
import time
import json
from common import read_config, read_resources, write_resources
from common import write_error, write_info
from common import get_map_value, set_map_value
from common import run_process, read_process
from common import save_terraform_state
from common import is_macos, is_windows, is_linux
from mgmt_station import env_terraform, init_terraform

def create(config_path, resources_path):
    # Read config and resources
    config = read_config(config_path)
    resources = read_resources(resources_path)

    environment_prefix = get_map_value(config, "environment.prefix")
    mgmt_keypair_name = environment_prefix + "-mgmt-station"
    path = os.path.abspath(os.path.dirname(__file__))

    # Set terraform env variables and init terraform folder
    tf_vars = env_terraform(config, resources)
    mgmt_terraform_path = init_terraform(environment_prefix)

    # Run terraform scripts
    os.chdir(mgmt_terraform_path)
    exit_code = run_process("terraform init")
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't initialize terraform. Watch logs above or check " + mgmt_terraform_path +" folder content.")

    exit_code = run_process("terraform plan " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't execute terraform plan. Watch logs above or check " + mgmt_terraform_path +" folder content.")
    
    exit_code = run_process("terraform apply -auto-approve " + tf_vars)
    if exit_code != 0:
        os.chdir(path + "/..")
        write_error("mgmt_station", "Can't create cloud resources. Watch logs above or check " + mgmt_terraform_path +" folder content.")

    # Get terraform outputs
    outputs = json.loads(str(read_process("terraform output -json")))
    mgmt_private_ip = outputs["private_ip"]["value"][0][0]
    mgmt_public_ip = outputs["public_ip"]["value"][0][0]

    os.chdir(path + "/..")

    save_terraform_state("mgmt_station", environment_prefix)

    set_map_value(resources, "mgmt_station.key_name", mgmt_keypair_name)
    set_map_value(resources, "mgmt_station.public_ip", mgmt_public_ip)
    set_map_value(resources, "mgmt_station.private_ip", mgmt_private_ip)
    write_resources(resources_path, resources)

    write_info ("mgmt_station", "Waiting 2 minutes while mgmt station is initializing...")
    time.sleep(120)

    if get_map_value(config, "mgmt_station.publicly_available") == True: 
        mgmt_ip = mgmt_public_ip
    else:
        mgmt_ip = mgmt_private_ip

    # Copy whole project to mgmt station
    if get_map_value(config, "mgmt_station.copy_project_to_mgmt_station") == True: 
        mgmt_instance_username = get_map_value(config, "mgmt_station.instance_username")

        run_process("ssh -o StrictHostKeyChecking=accept-new " + mgmt_instance_username + "@" + mgmt_ip + \
            " -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + \
            ".pem \"mkdir -p /home/"+ mgmt_instance_username + "/env-test2-python\"" 
        )
        print("Copying files from local to mgmt station...")

        if is_windows():
            run_process ("scp -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + ".pem" + \
                " -r " + path + "/../* " + mgmt_instance_username + "@" + mgmt_ip + ":/home/"+ mgmt_instance_username + "/env-test2-python"
            )
        else:
            # linux or macos
            run_process ("scp -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + ".pem" + \
                " -r " + path + "/.. " + mgmt_instance_username + "@" + mgmt_ip + ":/home/"+ mgmt_instance_username
            )
        write_info ("mgmt_station", "Copying files from local to mgmt station is done")

        # Save ssh commands
        ssh_command = "ssh -i " + mgmt_terraform_path + "/secrets/" + mgmt_keypair_name + ".pem " + \
            mgmt_instance_username + "@" + mgmt_ip
        set_map_value(resources, "mgmt_station.ssh_command", ssh_command)
        write_resources(resources_path, resources)

        write_info ("mgmt_station", "Mgmt station ssh command: " + ssh_command, False, "Green")

    else:
        write_info("mgmt_station", "Copying project to mgmt station skipped because it is disabled in config file.")
